Personal Ansible Pull
======================

## Gitlab

### Issues

- [X] Server gave bad signature for ssh key 0
- [X] Failed to checkout branch main with ansible-pull
- [ ] 'vagrant' is configured as our ansible_user (hosts) but we should use 'alexa'
- [ ] Clarify who should run the ansible playbook by default
- [ ] Make sure our cron scripts (backup VM definition, etc.) use the proper logging feature instead of ad-hoc
- [X] Configure properly /home/_config
- [ ] Add users to extra groups (sylvain to qemu)
- [X] Install the ansible_role locally for ansible_pull
- [X] Update provision script to clone repo in /home/_config/VAR_LIB
- [X] Check that ansible reset cron job is correct since we changed where ansible-pull
  download repo
- [X] ansible-pull: error with libvirt virt_net and virt_pool
- [X] ansible-playbook: error with libvirt virt_net and virt_pool
- [ ] Check how ansible-pull handle reboot (not supported see
  https://github.com/ansible/ansible/issues/57874)

