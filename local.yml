---

# =============================================================================
# Setup the system
# =============================================================================

- name: System Update
  hosts: all
  tags: always
  become: true

  pre_tasks:
    - import_tasks: tasks/set_sylvain_facts.yml

  roles:
    - system_update_latest



- name: System Setup
  hosts: all
  become: true
  tags: setup

  roles:

    - debug_conf
    # - { role: debug_conf, when: (debug_script is defined) and debug_script }

    - { role: users_add, tags: users }

    - { role: install_support_flatpak, tags: always,  when: (need_support_flatpak is defined) and need_support_flatpak }
    - { role: install_support_snap,    tags: always,  when: (need_support_snap is defined) and need_support_snap }
    - { role: install_support_aur,     tags: always,  when: (need_support_aur is defined) and need_support_aur and (sylvain_facts.os_family == "archlinux") }

    - { role: update_list_packages_installed, tags: packages }

    # minimum software to install
    - { role: apps_minimal, tags: ['package', 'minimal'] }

    # configure a bunch of stuff
    - configure_time
    - configure_console
    - { role: configure_cron,   tags: ['packages', 'cron'] }
    - configure_etc_host
    - { role: configure_grub,   tags: ['packages', 'grub'] }
    - configure_home_config
    - configure_hostname
    - configure_locale
    - configure_repositories
    - configure_sudo
    - { role: configure_usr_local, tags: usrlocal }
    
    # host specific tasks
    - host_specific_tasks
    - hardware_specific_tasks



- name: Install Packages
  hosts: all
  become: true
  tags: packages
  roles:

     # install apps
    - { role: apps_greeter_common,    tags: ['greeter', 'dektop'] }
    - { role: apps_greeter_gdm,       tags: ['greeter', 'desktop', 'gdm'],      when: need_apps_greeter_gdm     is defined  and need_apps_greeter_gdm }
    - { role: apps_greeter_lightdm,   tags: ['greeter', 'desktop', 'lightdm'],  when: need_apps_greeter_lightdm is defined  and need_apps_greeter_lightdm }

    - { role: apps_desktop_common,    tags: desktop }
    - { role: apps_desktop_awesome,   tags: ['desktop','awesome'],  when: need_apps_desktop_awesome   is defined  and need_apps_desktop_awesome }
    - { role: apps_desktop_bspwm,     tags: ['desktop','bspwm'],    when: need_apps_desktop_bspwm     is defined  and need_apps_desktop_bspwm }
    - { role: apps_desktop_cinnamon,  tags: ['desktop','cinnamon'], when: need_apps_desktop_cinnamon  is defined  and need_apps_desktop_cinnamon }
    - { role: apps_desktop_gnome,     tags: ['desktop','gnome'],    when: need_apps_desktop_gnome     is defined  and need_apps_desktop_gnome }
    - { role: apps_desktop_mate,      tags: ['desktop','mate'],     when: need_apps_desktop_mate      is defined  and need_apps_desktop_mate }
    - { role: apps_desktop_xfce,      tags: ['desktop','xfce'],     when: need_apps_desktop_xfce      is defined  and need_apps_desktop_xfce }

    - { role: apps_gnome_keyring,     tags: keyring }

    - { role: apps_dev_base,          tags: dev,              when: need_apps_dev_base    is defined  and need_apps_dev_base }
    - { role: apps_dev_lua,           tags: ['dev','lua'],    when: need_apps_dev_lua     is defined  and need_apps_dev_lua }
    - { role: apps_dev_python,        tags: ['dev','python'], when: need_apps_dev_python  is defined  and need_apps_dev_python }

    - { role: apps_children,          tags: children,                 when: need_apps_children          is defined  and need_apps_children }
    - { role: apps_emulators,         tags: emulators,                when: need_apps_emulators         is defined  and need_apps_emulators }
    - { role: apps_filemanagers,      tags: filemanagers,             when: need_apps_filemanagers      is defined  and need_apps_filemanagers }
    - { role: apps_fonts,             tags: fonts,                    when: need_apps_fonts             is defined  and need_apps_fonts }
    - { role: apps_games,             tags: games,                    when: need_apps_games             is defined  and need_apps_games }
    - { role: apps_games_extra,       tags: ['games','extra'],        when: need_apps_games_extra       is defined  and need_apps_games_extra }
    - { role: apps_internet,          tags: internet,                 when: need_apps_internet          is defined  and need_apps_internet }
    - { role: apps_internet_extra,    tags: ['internet','extra'],     when: need_apps_internet_extra    is defined  and need_apps_internet_extra }
    - { role: apps_lang_chinese,      tags: ['lang','chinese'],       when: need_apps_lang_chinese      is defined  and need_apps_lang_chinese }
    - { role: apps_lang_french,       tags: ['lang','french'],        when: need_apps_lang_french       is defined  and need_apps_lang_french }
    - { role: apps_lang_spanish,      tags: ['lang','spanish'],       when: need_apps_lang_spanish      is defined  and need_apps_lang_spanish }
    - { role: apps_mail_local,        tags: mail,                     when: need_apps_mail_local        is defined  and need_apps_mail_local }
    - { role: apps_multimedia,        tags: multimedia,               when: need_apps_multimedia        is defined  and need_apps_multimedia }
    - { role: apps_network,           tags: network,                  when: need_apps_network           is defined  and need_apps_network }
    - { role: apps_office,            tags: office,                   when: need_apps_office            is defined  and need_apps_office }
    - { role: apps_printers_pdf,      tags: ['printers','pdf'],       when: need_apps_printers_pdf      is defined  and need_apps_printers_pdf }
    - { role: apps_printers_hardware, tags: ['printers','hardware'],  when: need_apps_printers_hardware is defined  and need_apps_printers_hardware }
    - { role: apps_tui,               tags: tui,                      when: need_apps_tui               is defined  and need_apps_tui }
    - { role: apps_utilities,         tags: utilities,                when: need_apps_utilities         is defined  and need_apps_utilities }

    # firewall need to be last because some servition must happen after all
    # services have been installed.
    - { role: apps_firewall,          tags: firewall,     when: need_apps_firewall }




- name: post-Install
  hosts: all
  become: true
  tags: post
  roles:
    - update_list_packages_installed

    # post-install scripts
    - post_install_system
    - { role: post_install_users, tags: users }

    - system_cleanup


# =============================================================================
# Reporting
# =============================================================================
# Notes:
# - These are local tasks, run with the current user.
# - Do not request to become root
# =============================================================================

- name: Report
  hosts: all
  tags: always
  tasks:
    - import_tasks: tasks/send_completion_alert.yml
      when: (task_failed is not defined)

    - import_tasks: tasks/send_failure_alert.yml
      when: (task_failed is defined) and task_failed

