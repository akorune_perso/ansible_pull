#!/bin/sh

SRC="/etc/libvirt/qemu"
LIBVIRT="/home/_config/VAR_LIB/libvirt"

DBCK="$LIBVIRT/machines_bck"
DLOG="$LIBVIRT/log"

for f in "$DLOG" "$DBCK"; do
    if [ ! -d "$f" ]; then
        mkdir -p "$f"
    fi
done

date=$(date '+%F_%H-%M-%S')
logfile="$DLOG/${date}_error.log"


rsync "$SRC"/*.xml "$DBCK" 2> "$logfile"
ret=$?
if [ "$ret" = "0" ]; then
    # everything went well, we can remove the log
    rm -f "$logfile"
fi

