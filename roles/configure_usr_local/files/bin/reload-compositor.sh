#!/bin/sh

# -----------------------------------------------------------------------------
# Reload the compositor process or start if it is not running
# -----------------------------------------------------------------------------

compositor=picom
compositor_name=Picom
compositor_cmd="picom -b"

# reload .Xresources
XRESOURCE_FILE="$HOME/.Xresources"
if [ -r "$XRESOURCE_FILE" ]; then
    xrdb merge "$XRESOURCE_FILE"
fi

if pgrep -f "${compositor}"; then
    pkill -USR1 -x "${compositor}" \
        && notify-send "Compositor" "Reloaded ${compositor_name}"
else
    ${compositor_cmd} &
    notify-send "Compositor" "Started ${compositor_name}"
fi
