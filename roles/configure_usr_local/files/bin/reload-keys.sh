#!/bin/sh

# -----------------------------------------------------------------------------
# Reload the key bindings daemon process or start it if it is not running
# -----------------------------------------------------------------------------

keybinding=sxhkd
keybinding_name=Sxhkd
keybinding_cmd=""

# reload .Xresources
XRESOURCE_FILE="$HOME/.Xresources"
if [ -r "$XRESOURCE_FILE" ]; then
    xrdb merge "$XRESOURCE_FILE"
fi

if pgrep -f "${keybinding}"; then
    pkill -USR1 -x "${keybinding}" \
        && notify-send "Key Bindings" "Reloaded ${keybinding_name}"
else
    "${keybinding_cmd}" &
    notify-send "Key Bindings" "Started ${keybinding_name}"
fi
