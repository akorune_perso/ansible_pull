#!/bin/bash

SXHKD_DIR=$HOME/.config/sxhkd

# Provides sane defaults if we are using an unknown WM. This is an array
# because in general we will aggregate and process several files.
SRC=("$SXHKD_DIR"/sxhkdrc)

case $DESKTOP_SESSION in
    bspwm)
        SRC=("$SXHKD_DIR"/sxhkdrc.{apps,bspwm})
        ;;
    gnome)
        SRC=("$SXHKD_DIR"/sxhkdrc.{apps,gnome})
        ;;
    xfce)
        SRC=("$SXHKD_DIR"/sxhkdrc.{apps,xfce})
        ;;
esac

awk '/^[a-z]/ && last {print $0,"\t",last} {last=""} /^#/{last=$0}' "${SRC[@]}" | column -t -s $'\t' | sort

# awk '/^[a-z]/ && last {print "<small>",$0,"\t",last,"</small>"} {last=""} /^#/{last=$0}' "${SRC[@]}" |
#     column -t -s $'\t' | sort |
#     rofi -dmenu -i -markup-rows -no-show-icons -width 60
