#!/bin/bash

# ============================================================================
# Inkscape configuraton
# ============================================================================

# button  1: undo
xsetwacom --set "HUION Huion Tablet_HS611 Pad pad" Button  1 "key +ctrl z"
# button  2: pencil tool
xsetwacom --set "HUION Huion Tablet_HS611 Pad pad" Button  2 "key f6"
# button  3: calligraphy tool
xsetwacom --set "HUION Huion Tablet_HS611 Pad pad" Button  3 "key +ctrl f6"
# button  4: bucket tool
xsetwacom --set "HUION Huion Tablet_HS611 Pad pad" Button  8 "key +shift f7"
# button  5: zoom out
xsetwacom --set "HUION Huion Tablet_HS611 Pad pad" Button  9 "key +shift ="
# button  6: zoom in
xsetwacom --set "HUION Huion Tablet_HS611 Pad pad" Button 10 "key -"
# button  7: adjust image to drawing
xsetwacom --set "HUION Huion Tablet_HS611 Pad pad" Button 11 "key 4"
# button  8: 
xsetwacom --set "HUION Huion Tablet_HS611 Pad pad" Button 12 "key +ctrl +shift f"
# button  9: eraser tool 
xsetwacom --set "HUION Huion Tablet_HS611 Pad pad" Button 13 "key +shift e"
# button 10: add a new layer
xsetwacom --set "HUION Huion Tablet_HS611 Pad pad" Button 14 "key +ctrl +shift n"

