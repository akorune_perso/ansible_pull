#!/bin/bash

# ============================================================================
# Gimp configuraton
# ============================================================================

# button  1: undo
xsetwacom --set "HUION Huion Tablet_HS611 Pad pad" Button  1 "key +ctrl +z -z -ctrl"
# button  2: pencil tool
xsetwacom --set "HUION Huion Tablet_HS611 Pad pad" Button  2 "key n"
# button  3: airbrush tool
xsetwacom --set "HUION Huion Tablet_HS611 Pad pad" Button  3 "key a"
# button  4: bucket tool
xsetwacom --set "HUION Huion Tablet_HS611 Pad pad" Button  8 "key +shift b"
# button  5: zoom out
xsetwacom --set "HUION Huion Tablet_HS611 Pad pad" Button  9 "key +shift ="
# button  6: zoom in
xsetwacom --set "HUION Huion Tablet_HS611 Pad pad" Button 10 "key -"
# button  7: adjust image to window
xsetwacom --set "HUION Huion Tablet_HS611 Pad pad" Button 11 "key +ctrl +shift j -ctrl"
# button  8: 
#xsetwacom --set "HUION Huion Tablet_HS611 Pad pad" Button 12 "key "
# button  9: eraser tool 
xsetwacom --set "HUION Huion Tablet_HS611 Pad pad" Button 13 "key +shift e"
# button 10: add a new layer
xsetwacom --set "HUION Huion Tablet_HS611 Pad pad" Button 14 "key +ctrl +shift n -ctrl"

