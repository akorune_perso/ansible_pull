#!/bin/sh

ROFI_PROMPT=
ROFI_SCRIPT=/usr/local/bin/list-mount-roach.sh
ROFI_THEME=mount-roach

# -----------------------------------------------------------------------------

theme_short="themes/scripts/$ROFI_THEME"
theme_full="$HOME/.config/rofi/$theme_short.rasi"

opt_theme=
if [ -r "$theme_full" ]; then
    opt_theme="-theme $theme_short"
fi

rofi -modi $ROFI_PROMPT:$ROFI_SCRIPT -show $ROFI_PROMPT $opt_theme

