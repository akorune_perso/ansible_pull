#! /bin/sh -

# -----------------------------------------------------------------------------
# Reload the .Xresources file with notification
# -----------------------------------------------------------------------------

PROGNAME=$0
XRESOURCE_FILE="$HOME/.Xresources"

# -----------------------------------------------------------------------------

notification=false
message="Loaded .Xresources"

# -----------------------------------------------------------------------------
# Functions

usage() {
  cat << EOF >&2
Usage: $PROGNAME [-n] [-m <message>]
 -n:            Send a notification message.
 -m <message>:  Use <message> for the notification (only when -n is used).
EOF
  exit 2
}


# -----------------------------------------------------------------------------
# Main
# -----------------------------------------------------------------------------

OPTS=$(getopt -o 'nm:' -a -l 'notify,message:' -n "$PROGNAME" -- "$@")
VALID_ARGUMENTS=$?
if [ "$VALID_ARGUMENTS" != "0" ]; then
  usage
fi

set -- $OPTS
while true; do
    case "$1" in
        -n | --notify)
            notification=true
            shift
            ;;
        -m | --message)
            message=$2
            shift 2
            ;;
        --) 
            shift
            break
            ;;
        # If invalid options were passed, then getopt should have reported an error,
        # which we checked as VALID_ARGUMENTS when getopt was called...
        *) 
            printf "Unexpected option: $1"
            usage
            ;;
    esac
done

if [ -r "$XRESOURCE_FILE" ]; then
    xrdb merge "$XRESOURCE_FILE"
    if [ "$notification" = "true" ]; then
        notify-send "Xresources" "$message"
    fi
fi

