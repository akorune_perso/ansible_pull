#!/bin/sh

# =============================================================================
# Launch appropriate terminal
# =============================================================================

# If the user has defined some environment variables regarding his preferred 
# terminal under a specific desktop environment, we respect his default, else 
# we will pick a default for him.

default_terminal="$TERMINAL"
default_terminal_awesome="$TERMINAL_AWESOME"
default_terminal_bspwm="$TERMINAL_BSPWM"
default_terminal_cinnamon="$TERMINAL_CINNAMON"
default_terminal_gnome="$TERMINAL_GNOME"
default_terminal_kde="$TERMINAL_KDE"
default_terminal_mate="$TERMINAL_MATE"
default_terminal_xfce="$TERMINAL_XFCE"
default_terminal_fallback="$TERMINAL_FALLBACK"

[ "$default_terminal" = "" ] && default_terminal="xterm"
[ "$default_terminal_awesome" = "" ] && default_terminal_awesome="xterm"
[ "$default_terminal_bspwm" = "" ] && default_terminal_bspwm="xterm"
[ "$default_terminal_cinnamon" = "" ] && default_terminal_cinnamon="gnome-terminal"
[ "$default_terminal_kde" = "" ] && default_terminal_kde="konsole"
[ "$default_terminal_gnome" = "" ] && default_terminal_gnome="gnome-terminal"
[ "$default_terminal_mate" = "" ] && default_terminal_mate="mate-terminal"
[ "$default_terminal_xfce4" = "" ] && default_terminal_xfce4="xfce4-terminal"
[ "$default_terminal_fallback" = "" ] && default_terminal_fallback="xterm"


# Search for the right terminal
if [ "$TERMINAL" != "" ]; then
    cmd="$TERMINAL"
else
    case $DESKTOP_SESSION in 
        awesome)
            conf="$HOME/.config/awesome/rc.lua"
            cmd=""
            if [ -f "$conf" ]; then
                cmd=$(cat "$conf" | grep -E -i -e "^(local)\s+terminal\s+=" | cut -d= -f2 | tr -d " '\"")
            fi
            if [ "$cmd" = "" ]; then
                cmd="$default_terminal_awesome"
            fi
            ;;
        bspwm)
            cmd="$default_terminal_bspwm"
            ;;
        cinnamon|cinnamon2d)
            cmd=$(gsettings get 'org.cinnamon.desktop.default-applications.terminal' 'exec' > /dev/null 2>&1)
            if [ "$cmd" = "" ]; then
                cmd="$default_terminal_cinnamon"
            fi
            ;;
        gnome|gnome-wayland|gnome-xorg|ubuntu-xorg|ubuntu-wayland)
            cmd=$(gsettings get 'org.gnome.desktop.default-applications.terminal' 'exec' > /dev/null 2>&1 )
            if [ "$cmd" = "" ]; then
                cmd="$default_terminal_gnome"
            fi
            ;;
        kde)
            cmd="$default_terminal_kde"
            ;;
        mate)
            cmd=$(gsettings get 'org.mate.desktop.default-applications.terminal' 'exec' > /dev/null 2>&1)
            if [ "$cmd" = "" ]; then
                cmd="$default_terminal_mate"
            fi
            ;;
        xfce)
            # Notes:
            # - this doesn't work if the user has defined a custom or not well recognized
            #   terminal emulator such as kitty.
            conf="$HOME/.config/xfce4/helpers.rc"
            cmd=""
            if [ -f "$conf" ]; then
                cmd=$(cat "$conf" | grep TerminalEmulator | cut -d= -f2)
            fi
            if [ "$cmd" = "" ]; then
                cmd="$default_terminal_xfce"
            fi
            ;;
        *)
            cmd="$default_terminal_fallback"
            ;;
    esac
fi
printf "$cmd"

