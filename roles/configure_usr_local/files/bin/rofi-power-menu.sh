#!/bin/sh

ROFI_PROMPT=Power
ROFI_SCRIPT=/usr/local/bin/list-power-menu.sh
ROFI_THEME=power-menu

# -----------------------------------------------------------------------------

theme_short="themes/scripts/$ROFI_THEME"
theme_full="$HOME/.config/rofi/$theme_short.rasi"

opt_theme=
if [ -r "$theme_full" ]; then
    opt_theme="-theme $theme_short"
fi

# Notes:
# - rows are 0-indexed, to pre-select row 3, you need to use '2'
rofi -modi $ROFI_PROMPT:$ROFI_SCRIPT -show $ROFI_PROMPT $opt_theme -selected-row 2

