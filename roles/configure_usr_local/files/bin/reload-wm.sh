#!/bin/sh

# -----------------------------------------------------------------------------
# Reload the window manager process or start if it is not running
# -----------------------------------------------------------------------------

wm=bspwm
wm_name=Bspwm
wm_cmd="$HOME/.config/bspwm/bspwmrc"

# reload .Xresources
XRESOURCE_FILE="$HOME/.Xresources"
if [ -r "$XRESOURCE_FILE" ]; then
    xrdb merge "$XRESOURCE_FILE"
fi

if pgrep -f "${wm}"; then
    "${wm_cmd}" &
    notify-send "Window Manager" "Reloaded ${wm_name}"
else
    "${wm_cmd}" &
    notify-send "Window Manager" "Started ${wm_name}"
fi
