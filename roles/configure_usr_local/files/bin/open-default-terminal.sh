#!/bin/sh

# =============================================================================
# Launch appropriate terminal
# =============================================================================

terminal=$(get-default-terminal.sh)

if command -v $terminal > /dev/null 2>&1; then
    exec "$terminal"
else
    printf "The default terminal should be '$terminal' but the command was not found!" > /dev/stderr
fi

