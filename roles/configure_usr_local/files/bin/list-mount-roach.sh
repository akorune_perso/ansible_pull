#!/bin/sh

arg="$1"

NAME=Roach
SERVER=roach

if [ -z "$arg" ]; then
    # no arguments provided, we assume we want to display the menu
    echo -en "$NAME » Home\0icon\x1fuser-home\n"
    echo -en "$NAME » Homes\0icon\x1fuser-home\n"
    echo -en "$NAME » Family\0icon\x1ffolder-publicshare\n"
    echo -en "$NAME » Music\0icon\x1ffolder-music\n"
    echo -en "$NAME » Photo\0icon\x1ffolder-pictures\n"
    echo -en "$NAME » Video\0icon\x1ffolder-videos\n"
    echo -en "$NAME » Www\0icon\x1fnetwork-workgroup\n"
else
    case $arg in
        "$NAME » Home")
            mount_gio.sh "smb://$SERVER/home" "$arg"	
            ;;
        "$NAME » Homes")
            mount_gio.sh "smb://$SERVER/homes" "$arg"	
            ;;
        "$NAME » Family")
            mount_gio.sh "smb://$SERVER/family" "$arg"	
            ;;
        "$NAME » Music")
            mount_gio.sh "smb://$SERVER/music" "$arg"	
            ;;
        "$NAME » Photo")
            mount_gio.sh "smb://$SERVER/photo" "$arg"
            ;;
        "$NAME » Video")
            mount_gio.sh "smb://$SERVER/video" "$arg"	
            ;;
        "$NAME » Www")
            mount_gio.sh "smb://$SERVER/web" "$arg"	
            ;;
    esac
fi
