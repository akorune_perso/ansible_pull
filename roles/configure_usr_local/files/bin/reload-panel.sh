#!/bin/sh

# -----------------------------------------------------------------------------
# Reload the panel process or start if it is not running
# -----------------------------------------------------------------------------

panel=polybar
panel_name=Polybar
panel_cmd="$HOME/.config/polybar/launch.sh"

# reload .Xresources
XRESOURCE_FILE="$HOME/.Xresources"
if [ -r "$XRESOURCE_FILE" ]; then
    xrdb merge "$XRESOURCE_FILE"
fi

if pgrep -f "${panel}"; then
    pkill -USR1 -x "${panel}" \
        && notify-send "Panel" "Reloaded ${panel_name}"
else
    "${panel_cmd}" &
    notify-send "Panel" "Started ${panel_name}"
fi
