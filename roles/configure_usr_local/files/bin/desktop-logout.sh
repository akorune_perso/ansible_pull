#!/bin/sh

# Close all windows gracefully for Awesome
awesome_close() {
    awesome-close.sh
    sleep .5
}

# Close all windows gracefully for BSPWM
bspwm_close() {
    bspwm-close.sh
    sleep .5
}

# Finally
case $DESKTOP_SESSION in
    awesome)
        # running Awesome WM
        awesome_close
        # logout
        echo 'awesome.quit()' | awesome-client
        ;;
    bspwm)
        # running BSPWM only
        bspwm_close
        # logout
        bspc quit
        ;;
    xfce)
        # running Xfce with BSPWM as window manager
        bspwm_close
        # logout without saving the session
        if pgrep -f polybar ; then
            pkill polybar
        fi
        xfce4-session-logout --logout --fast
        ;;
    mate)
        # running Mate with BSPWM as window manager
        bspwm_close
        if pgrep -f polybar ; then
            pkill polybar
        fi
        mate-session-save --logout 
        ;;
    *)
        # running something else
        ;;
esac
