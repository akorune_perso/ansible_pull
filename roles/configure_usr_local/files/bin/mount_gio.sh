#!/bin/bash

################################################################################
# Mount a remote folder in gnome using gio.
#
# Parameters
#	addr : string
#		the address to use (for example: 'smb://server/share')
#	name : string
#		the name to be used (for example 'share on server')
################################################################################

# extract the name of the command being run
_self="${0##*/}"
 
# command usage
Usage(){
    echo "Syntax: $_self addr [name]"
}

################################################################################

# convert the command lines arguments into an array
args=( "$@" )
args_len=${#args[@]}

# if only 1 argument is provided, assume it is the addr, and that we want to
# extract the name from the addr.
if [ "$args_len" -eq 1 ]; then
	args+=( ${args[0]} )
	args_len=${#args[@]}
else
	if [ "$args_len" -eq 0 ] || [ "$args_len" -gt 2 ]; then
		Usage
		exit 1
	fi
fi

addr=${args[0]}
name=${args[1]}


errormsg=$(gio mount $addr 2>&1)
status=$?

# echo $status
# echo $errormsg

if [ $status -ne 0 ]; then
	if [[ $errormsg == *"Location is already mounted"* ]]; then
		notify-send -u low -i dialog-information "Mount $name" "Your directory is already mounted!"
	else
		zenity --error --no-wrap --text "Unable to mount $name!\n\n$errormsg"
	fi
else
	notify-send -u low -i dialog-information "Mount $name" "Mounted your directory."
fi
