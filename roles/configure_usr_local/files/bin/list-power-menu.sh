#!/bin/sh

arg="$1"

if [ -z "$arg" ]; then
    # no arguments provided, we assume we want to display the menu
    echo -en "Logout\0icon\x1fsystem-log-out\n"
    echo -en "Reboot\0icon\x1fsystem-restart\n"
    echo -en "Shutdown\0icon\x1fsystem-shutdown\n"
    echo -en "Suspend\0icon\x1fsystem-suspend\n"
    echo -en "Hibernate\0icon\x1fsystem-hibernate\n"
else
    case $arg in
        Hibernate)
            systemctl hibernate
            ;;
        Logout)
            desktop-logout.sh
            ;;
        Reboot)
            desktop-logout.sh
            systemctl reboot
            ;;
        Shutdown)
            desktop-logout.sh
            systemctl poweroff
            ;;
        Suspend)
            systemctl suspend
            ;;
        *)
            echo "arg=$arg"
            ;;
    esac
fi
